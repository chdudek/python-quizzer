# python-quizzer

## Description

A small audience response system (ARS). Featuring a moderator view, player view and a spectator view.

The app is built with [Python](https://www.python.org), [Flask](https://flask.palletsprojects.com/), [SQLite](https://www.sqlite.org) and [socket.io](https://socket.io) in the backend and [jQuery](https://jquery.com) and [Spectre.css](https://picturepan2.github.io/spectre/) framework in the frontend.

## How to install

Best way to test the installation is using a Python virtual environment:

```bash
git clone https://gitlab.com/chdudek/python-quizzer
cd python-quizzer
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Initialize the database with `flask --app quizzer init-db`

## How to run development mode

```bash
cd python-quizzer
flask --app quizzer --debug run --host=0.0.0.0
```

Open [http://127.0.0.1/admin/] in your browser and log in as `Admin` with password `admin`.

Have fun!

## License

MIT License

## Todo

- [ ] User management
- [ ] Migrate to socket.io protocol revision 5
- [ ] Check email sending
