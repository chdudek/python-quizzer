import flask

from quizzer.db import get_db

bp = flask.Blueprint('client', __name__, url_prefix='/client')


@bp.route('/<game_id>')
def game(game_id):
    db = get_db()
    game = db.execute('SELECT * FROM game WHERE code = ?',
                      (game_id,)).fetchone()
    if game is None:
        return flask.abort(404)
    elif game['ended'] is not None:
        return f'Game {game_id} is finished!'
    elif game['started'] is not None:
        return f'Game {game_id} is running!'
    else:
        return flask.render_template('lobby.html', val={'game_id': game_id})
