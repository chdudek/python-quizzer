#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import time
import flask
import math
import random

from flask import request
import functools

import flask_socketio
from quizzer.sio import socketio as sio
from quizzer.db import get_db

bp = flask.Blueprint('game', __name__, url_prefix='/game')

strChars = "ABCDEFGHIJKLMNIOPQRSTUVWXYZ1234567890"


def check(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        pid = flask.session.get('player_id')
        gid = flask.session.get('game_id')
        if pid is not None and gid is not None:
            db = get_db()
            u = db.execute(
                'SELECT id FROM player WHERE id=? AND game_id=?',
                (pid, gid)
            ).fetchone()
            if u is None:
                del flask.session['player_id']
        return f(*args, **kwargs)
    return wrapper


def secure(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        if flask.session.get('player') is None:
            return 'xxx'
        return f(*args, **kwargs)
    return wrapper


@sio.on('connect', namespace='/game')
def connect():
    print('Connecting...')
    db = get_db()
    if flask.session.get('player_id') is not None:
        player = db.execute(
            'SELECT * FROM player WHERE id=?',
            (flask.session.get('player_id'),)
        ).fetchone()
        if player is None:
            return None
        game = db.execute(
            'SELECT * FROM game WHERE id=?', (player['game_id'],)
        ).fetchone()
        # flask_socketio.join_room(flask.session.get('game')['code'])
        flask_socketio.join_room(game['code'])
        # print('Joining with player_id')
    elif flask.session.get('game_id') is not None:
        game = db.execute(
            'SELECT * FROM game WHERE id=?', (flask.session.get('game_id'),)
        ).fetchone()
        # print('Joining with game_id')
        flask_socketio.join_room(game['code'])
    else:
        print(f'Toast: Game not found!')
        sio.emit('toast', {'type': 'error', 'msg': 'Game not found!'})


@sio.on('join', namespace='/game')
def join_game(name, color):
    db = get_db()
    game = db.execute('SELECT * FROM game WHERE id=?',
                      (flask.session.get('game_id'),)).fetchone()
    name = name.strip()
    if not re.match('#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})', color):
        sio.emit(
            'toast', {'type': 'error', 'msg': 'Please select a color!'},
            room=request.sid
        )
    elif not name:
        sio.emit(
            'toast', {'type': 'error', 'msg': 'Please select a name!'},
            room=request.sid
        )
    elif db.execute(
        'SELECT id FROM player WHERE name = ? AND game_id=?',
        (name, flask.session.get('game_id'))
    ).fetchone() is not None and not game['anonymous']:
        sio.emit(
            'toast', {'type': 'error', 'msg': 'Name already taken!'},
            room=request.sid
        )
    else:
        code = ''.join([random.choice(strChars) for _ in range(6)])
        pid = db.execute(
            'INSERT INTO player (name, color, code, game_id) '
            'VALUES (?, ?, ?, ?)',
            (name, color, code, flask.session.get('game_id'))
        ).lastrowid
        db.commit()

        flask.session['player_id'] = pid
        url = flask.url_for('game.game', game_code=game['code'],
                            player_code=code)
        sio.emit('goto', url, room=request.sid)
        sio.emit(
            'joined', [{'name': name, 'color': color}],
            room=game['code'],
            namespace='/game')
        sio.emit(
            'joined', [{'name': name, 'color': color}],
            # room=game['code'],
            namespace='/admin')


@sio.on('guess', namespace='/game')
def register_guess(answer):
    answer = int(answer)
    db = get_db()

    if flask.session.get('player_id') is None:
        # sio.emit('reload', 0, dctGuesses, room=request.sid)
        sio.emit('reload', 0, room=request.sid)
        sio.emit('msg', {'type': 'error', 'msg': 'One vote not counted!'},
                 namespace='/admin')
        sio.sleep(2)
        sio.emit('toast', {'type': 'error', 'msg': 'Please vote again!'},
                 room=request.sid)
    else:
        player = db.execute('SELECT * FROM player WHERE id=?',
                            (flask.session.get('player_id'),)).fetchone()

        game = db.execute('SELECT * FROM game WHERE id=?',
                          (player['game_id'],)).fetchone()

        question = db.execute(
            'SELECT * FROM question WHERE quiz_id=? LIMIT ?,1',
            (game['quiz_id'], game['progress'])
        ).fetchone()

        guess = db.execute(
            'SELECT * FROM guess WHERE '
            'player_id=? AND game_id=? AND question_id=?',
            (player['id'], game['id'], question['id'])
        ).fetchone()

        if game['status'] != 1:
            sio.emit('toast', {'type': 'error', 'msg': 'Question is not open!'},
                     room=request.sid)

        if game['status'] == 1 and guess is None:
            db.execute(
                'INSERT INTO guess '
                '(player_id, game_id, question_id, answer_id, registered) '
                'VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP)',
                (player['id'], game['id'], question['id'], answer)
            )
            sio.emit('toast', {'type': 'success', 'msg': 'Vote saved!'},
                     room=request.sid)
        elif game['status'] == 1 and guess['answer_id'] != answer:
            db.execute(
                'UPDATE guess SET answer_id=?, registered=CURRENT_TIMESTAMP '
                'WHERE player_id=? AND game_id=? AND question_id=?',
                (answer, player['id'], game['id'], question['id'])
            )
            sio.emit('toast', {'type': 'success', 'msg': 'Vote updated!'},
                     room=request.sid)
        db.commit()

        guesses = db.execute(
            'SELECT answer_id AS id, COUNT(answer_id) AS cnt FROM guess '
            'WHERE question_id=? AND game_id=? '
            'GROUP BY answer_id',
            (question['id'], game['id'])
        ).fetchall()

        dctGuesses = {g['id']: g['cnt'] for g in guesses}

        sio.emit('guess', dctGuesses, namespace='/admin')


@bp.route('/<game_code>')
@bp.route('/<game_code>/<player_code>')
def game(game_code, player_code=None):

    pid = flask.session.get('player_id')
    db = get_db()
    player = None
    if pid is not None:
        player = db.execute(
            'SELECT * FROM player WHERE id=?', (pid,)
        ).fetchone()
    elif player_code is not None:
        player = db.execute('SELECT * FROM player WHERE code=?',
                            (player_code,)).fetchone()

    game = db.execute(
        'SELECT g.*, q.title AS title, q.body AS body, '
        'COUNT(x.id) AS questions '
        'FROM game g '
        'LEFT JOIN quiz q on g.quiz_id=q.id '
        'LEFT JOIN question x on x.quiz_id=q.id '
        'WHERE code = ?',
        (game_code,)).fetchone()

    if game is None:
        return flask.abort(404)

    flask.session['game_id'] = game['id']

    if game['ended'] is not None and player is not None:
        questions = db.execute(
            'SELECT q.*, a.title AS answer, a.correct FROM game g '
            'LEFT JOIN question q ON q.quiz_id=g.quiz_id '
            'LEFT JOIN guess s ON s.question_id=q.id AND s.player_id=? '
            'LEFT JOIN answer a ON a.id=s.answer_id '
            'WHERE g.id=? '
            'GROUP BY q.id',
            (flask.session.get('player_id'), game['id'],)
        ).fetchall()

        corr = sum([_['correct'] for _ in questions if _['correct'] == 1])

        return flask.render_template(
            'results_player.html', game=game, questions=questions, correct=corr
        )
    if game['ended'] is not None and player is None:
        players = db.execute(
            'SELECT p.*, SUM(a.correct) AS points FROM player p '
            'LEFT JOIN guess g ON g.player_id=p.id '
            'JOIN answer a ON a.id=g.answer_id '
            'WHERE p.game_id=? '
            'GROUP BY p.id '
            'ORDER BY points DESC LIMIT 5', (game['id'],)
        ).fetchall()
        return flask.render_template(
            'results.html', game=game, players=players
        )

    elif game['started'] is not None:
        if player is None:
            flask.flash('You are not a player of this game!', category='error')
            return flask.redirect(flask.url_for('view.index',
                                                game_code=game_code))
        else:
            flask.session['player_id'] = player['id']

        question = db.execute(
            'SELECT * FROM question WHERE quiz_id=? LIMIT ?,1',
            (game['quiz_id'], game['progress'])
        ).fetchone()

        answers = db.execute(
            'SELECT a.*, COUNT(g.id) AS guess '
            'FROM answer a '
            'LEFT JOIN guess g on g.answer_id=a.id AND g.player_id=? '
            'WHERE a.question_id=? '
            'GROUP BY a.id', (player['id'], question['id'],)
        ).fetchall()

        if game['timer'] == -1:
            t = -1
        elif game['timer'] > 0:
            t = max(0, int(game['timer']-time.time()))
        else:
            t = '-'
        return flask.render_template(
            'game.html', game=game, question=question, answers=answers,
            timer=t
        )
    else:
        players = db.execute(
            'SELECT id, name, color FROM player WHERE game_id=?',
            (game['id'],)
        ).fetchall()
        return flask.render_template(
            'lobby.html', game=game, players=players, player=player
        )
