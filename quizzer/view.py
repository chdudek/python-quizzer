#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import flask
import time
from pyshorteners import Shortener
from requests.exceptions import ReadTimeout

from quizzer.db import get_db
import flask_socketio

bp = flask.Blueprint("view", __name__, url_prefix="/view")


@bp.route("/<game_code>")
@bp.route("/<game_code>/index.html")
def index(game_code):
    db = get_db()
    game = db.execute(
        "SELECT g.*, q.title AS title, q.body AS body, "
        "COUNT(x.id) AS questions "
        "FROM game g "
        "LEFT JOIN quiz q on g.quiz_id=q.id "
        "LEFT JOIN question x on x.quiz_id=q.id "
        "WHERE code = ?",
        (game_code,),
    ).fetchone()

    flask.session["game_id"] = game["id"]

    players = db.execute(
        "SELECT id, name, color FROM player WHERE game_id=?", (game["id"],)
    ).fetchall()

    if game is None:
        return flask.abort(404)
    elif game["ended"] is not None:
        if game["anonymous"]:
            players = db.execute(
                "SELECT * FROM player WHERE game_id=?", (game["id"],)
            ).fetchall()

            questions = db.execute(
                "SELECT q.*, a.title AS answer, count(s.id) AS correct "
                "FROM game g "
                "LEFT JOIN question q ON q.quiz_id=g.quiz_id "
                "LEFT JOIN answer a ON a.question_id=q.id AND a.correct=1 "
                "LEFT JOIN guess s ON s.answer_id=a.id "
                "WHERE g.id=? "
                "GROUP BY q.id",
                (game["id"],),
            ).fetchall()
            return flask.render_template(
                "view/results_anonymous.html",
                game=game,
                questions=questions,
                players=len(players),
            )
        else:
            players = db.execute(
                "SELECT p.*, SUM(a.correct) AS points FROM player p "
                "LEFT JOIN guess g ON g.player_id=p.id "
                "JOIN answer a ON a.id=g.answer_id "
                "WHERE p.game_id=? "
                "GROUP BY p.id "
                "ORDER BY points DESC LIMIT 5",
                (game["id"],),
            ).fetchall()

            place = 1
            cur = 0
            for i, player in enumerate(players):
                player = dict(player)
                if player["points"] < cur:
                    place += 1
                player["place"] = place
                cur = player["points"]
                players[i] = player
            return flask.render_template(
                "view/results.html", game=game, players=players
            )
    elif game["started"] is not None:
        question = db.execute(
            "SELECT * FROM question WHERE quiz_id=? LIMIT ?,1",
            (game["quiz_id"], game["progress"]),
        ).fetchone()

        answers = db.execute(
            "SELECT a.*, COUNT(g.id) AS guess "
            "FROM answer a "
            "LEFT JOIN guess g on g.answer_id=a.id "
            "WHERE a.question_id=? "
            "GROUP BY a.id",
            (question["id"],),
        ).fetchall()
        if game["timer"] == -1:
            t = -1
        elif game["timer"] > 0:
            t = max(0, int(game["timer"] - time.time()))
        else:
            t = "-"

        return flask.render_template(
            "view/game.html", game=game, question=question, answers=answers, timer=t
        )
    else:
        url = flask.url_for("game.game", game_code=game["code"], _external=True)
        if game["url"] is None:
            try:
                # shortener = Shortener().tinyurl
                # url = shortener.short(url)
                db.execute("UPDATE game SET url=? WHERE id=?", (url, game["id"]))
                db.commit()
            except ReadTimeout as e:
                print(e, file=sys.stderr)
        else:
            url = game["url"]

        return flask.render_template(
            "view/lobby.html", game=game, players=players, url=url
        )
