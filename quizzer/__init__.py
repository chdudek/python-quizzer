#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import flask
import flask_session
import eventlet

from . import db
from . import sio
from . import admin
from . import game
from . import view

eventlet.monkey_patch()

bp = flask.Blueprint("main", __name__)


@bp.route("/teams.html")
def random_teams():
    return flask.render_template("random.html")


@bp.route("/")
@bp.route("/index.html")
def index():
    return flask.render_template("index.html")


def create_app(test_config=None):
    app = flask.Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        SESSION_TYPE="filesystem",
        DATABASE=os.path.join(app.instance_path, "quizzer.sqlite"),
        HOST="0.0.0.0",
        DEBUG=True,
        TEMPLATES_AUTO_RELOAD=True,
        ENV="development",
    )

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    flask_session.Session(app)
    sio.socketio.init_app(app)

    app.register_blueprint(admin.bp)
    app.register_blueprint(game.bp)
    app.register_blueprint(view.bp)
    app.register_blueprint(bp)

    return app


app = create_app()
