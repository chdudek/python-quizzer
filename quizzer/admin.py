#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import sys
import ssl
import time
import flask
import random
import smtplib
import secrets
import datetime
import functools

from email.message import EmailMessage

from werkzeug.security import check_password_hash, generate_password_hash

from quizzer.db import get_db
from quizzer.sio import socketio

reMail = re.compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,})+$")

bp = flask.Blueprint("admin", __name__, url_prefix="/admin")

strChars = "ABCDEFGHIJKLMNIOPQRSTUVWXYZ1234567890"
strNum = "ABCDEFGHIJ"

STATUS_CLOSED = 0
STATUS_OPEN = 1
STATUS_DONE = 2


def sendmail(strTo, strSubj, strBody):
    with smtplib.SMTP("TODO", 587) as svr:
        try:
            ctx = ssl.create_default_context()
            svr.starttls(context=ctx)
            svr.login("TODO", "")
            msg = EmailMessage()
            msg["Subject"] = strSubj
            msg["From"] = "TODO"
            msg["To"] = strTo
            msg.set_content(strBody)

            svr.send_message(msg)

        except Exception as e:
            print(e, file=sys.stderr)
            return False
        finally:
            return True


def secure(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if flask.g.author is None:
            return flask.redirect(flask.url_for("admin.login"))
        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def get_admin():
    flask.g.version = ".".join([str(_) for _ in sys.version_info[:3]])
    author_id = flask.session.get("author_id")
    if author_id is None:
        flask.g.author = None
    else:
        flask.g.author = (
            get_db()
            .execute("SELECT * FROM author WHERE id = ?", (author_id,))
            .fetchone()
        )


@bp.route("/register.html", methods=("GET", "POST"))
def register():
    if flask.request.method == "POST":
        name = flask.request.form["name"]
        email = flask.request.form["email"]
        password = flask.request.form["password"]

        code = secrets.token_urlsafe(8)

        db = get_db()
        err = None

        if not name:
            err = "Username needed!"
        if not email:
            err = "Email needed!"
        if not reMail.search(email):
            err = f"Invalid Email {email}"
        if not password:
            err = "Password needed!"

        elif (
            db.execute("SELECT id FROM author WHERE name = ?", (name,)).fetchone()
            is not None
        ):
            err = f"Author {name} is already registered!"

        if err is None:
            db.execute(
                "INSERT INTO author (name, email, password, code) "
                "VALUES (?, ?, ?, ?)",
                (name, email, generate_password_hash(password), code),
            )

            db.commit()
            url = flask.url_for("admin.verify", code=code, _external=True)
            msg = (
                f"Hello {name},\n\n"
                "to verify your Email follow this link:\n\n"
                f"{url}\n\n"
                "Your Quizzer Team"
            )

            if sendmail(email, "Quizzer Email verification", msg):
                flask.flash("Please check your email for verification!")
                return flask.redirect(flask.url_for("admin.login"))
            else:
                err = "An Email error occured!"

        flask.flash(err)

    return flask.render_template("admin/register.html")


@bp.route("/verify-<code>.html", methods=("GET",))
def verify(code):
    db = get_db()

    res = db.execute("SELECT id FROM author WHERE code=?", (code,)).fetchone()

    if res is None:
        flask.flash("Verification Code not valid!", category="error")
    else:
        db.execute("UPDATE author SET code='', level=1 WHERE id=?", res)
        db.commit()
        flask.flash("You can now login!", category="success")

    return flask.redirect(flask.url_for("admin.login"))


@bp.route("/login.html", methods=("GET", "POST"))
def login():
    if flask.request.method == "POST":
        name = flask.request.form["name"]
        password = flask.request.form["password"]

        db = get_db()
        err = None
        author = db.execute("SELECT * FROM author WHERE name = ?", (name,)).fetchone()

        if author is None:
            err = "Name not found!"
        elif not check_password_hash(author["password"], password):
            err = "Password not correct!"
        elif author["level"] == 0:
            err = "Please verify your email first!"

        if err is None:
            flask.session.clear()
            flask.session["author_id"] = author["id"]
            return flask.redirect(flask.url_for("admin.index"))

        flask.flash(err)
    return flask.render_template("admin/login.html")


@bp.route("/game/<game_code>/")
@secure
def show_game(game_code):
    db = get_db()
    game = db.execute(
        "SELECT g.*, q.id AS quiz_id, q.title AS title, q.body AS body, "
        "COUNT(n.id) as questions "
        "FROM game g "
        "LEFT JOIN quiz q on g.quiz_id=q.id "
        "LEFT JOIN question n on n.quiz_id=q.id "
        "WHERE code = ? "
        "GROUP BY q.id",
        (game_code,),
    ).fetchone()

    question = db.execute(
        "SELECT * FROM question WHERE quiz_id=? LIMIT ?,1",
        (game["quiz_id"], game["progress"]),
    ).fetchone()

    answers = db.execute(
        "SELECT a.*, COUNT(g.id) AS guesses "
        "FROM answer a "
        "LEFT JOIN guess g ON g.answer_id=a.id AND g.game_id=? "
        "WHERE a.question_id=? "
        "GROUP BY a.id",
        (game["id"], question["id"]),
    ).fetchall()

    flask.session["game_id"] = game["id"]

    if game is None:
        del flask.session["game_id"]
        return flask.abort(404)
    elif game["ended"] is not None:
        players = db.execute(
            "SELECT p.*, SUM(a.correct) AS points FROM player p "
            "LEFT JOIN guess g ON g.player_id=p.id "
            "JOIN answer a ON a.id=g.answer_id "
            "WHERE p.game_id=? "
            "GROUP BY p.id "
            "ORDER BY points DESC ",
            (game["id"],),
        ).fetchall()

        return flask.render_template("admin/results.html", game=game, players=players)
    elif game["started"] is None:
        players = db.execute(
            "SELECT id, name, color FROM player WHERE game_id=?", (game["id"],)
        ).fetchall()
        return flask.render_template(
            "admin/lobby.html",
            game=game,
            question=question,
            answers=answers,
            players=players,
        )
    return flask.render_template(
        "admin/game.html", game=game, question=question, answers=answers
    )


@bp.route("/game/start-<game_id>.html")
@secure
def start_game(game_id):
    db = get_db()
    game = db.execute("SELECT * FROM game WHERE id = ?", (game_id,)).fetchone()

    if game is None:
        del flask.session["game_id"]
        return flask.redirect(flask.url_for("admin.index"))

    if game["started"] is None:
        db.execute(
            "UPDATE game SET started=CURRENT_TIMESTAMP " "WHERE id=?", (game["id"],)
        )
        db.commit()
        flask.session["game_id"] = game_id

        socketio.emit("reload", 0, room=game["code"], namespace="/game")

    return flask.redirect(flask.url_for("admin.show_game", game_code=game["code"]))


@bp.route("/game/delete-<game_id>.html")
@secure
def del_game(game_id):
    db = get_db()
    db.execute("DELETE FROM game WHERE id=?", (game_id,))
    db.execute("DELETE FROM player WHERE game_id=?", (game_id,))
    db.execute("DELETE FROM guess WHERE game_id=?", (game_id,))
    db.commit()

    flask.flash("Game deleted!", category="info")
    return flask.redirect(flask.url_for("admin.index"))


@bp.route("/quiz/new.html")
@secure
def new_quiz():
    return flask.render_template("admin/new_quiz.html")


@bp.route("/quiz/add.html", methods=("POST",))
@secure
def add_quiz():
    dctData = flask.request.get_json()
    # print(dctData, file=sys.stderr)

    if dctData["title"].strip() == "":
        return flask.jsonify({"status": "error", "msg": "Title is needed!"})
    elif len(dctData["questions"]) == 0:
        return flask.jsonify(
            {"status": "error", "msg": "At least 1 question is needed!"}
        )

    db = get_db()

    # Create quiz
    quiz_id = db.execute(
        "INSERT INTO quiz (title, body, author_id, public) " "VALUES (?, ?, ?, ?)",
        (dctData["title"], dctData["body"], flask.g.author["id"], dctData["public"]),
    ).lastrowid

    # Create questions
    for question in dctData["questions"]:
        qid = db.execute(
            "INSERT INTO question (title, quiz_id) VALUES (?, ?)",
            (question["title"], quiz_id),
        ).lastrowid

        # Create answers
        for answer in question["answers"]:
            db.execute(
                "INSERT INTO answer (title, correct, question_id) " "VALUES (?, ?, ?)",
                (answer[0], answer[1], qid),
            )

    db.commit()

    return flask.jsonify(
        {"status": "success", "msg": "Quiz successfully added!", "quiz_id": quiz_id}
    )


@bp.route("/quiz/delete-<quiz_id>.html")
@secure
def del_quiz(quiz_id):
    db = get_db()

    a = db.execute("SELECT author_id FROM quiz " "WHERE id=?", (quiz_id,)).fetchone()[0]

    if a != flask.g.author["id"]:
        flask.flash("This is not your quiz!", category="error")
        return flask.redirect(flask.url_for("admin.index", _anchor="quiz"))

    cur = db.execute("SELECT id FROM question WHERE quiz_id=?", (quiz_id,))
    question_ids = [_[0] for _ in cur.fetchall()]

    lstIds = ",".join(["?"] * len(question_ids))

    cur = db.execute(
        f"SELECT id FROM answer WHERE question_id IN ({lstIds})", question_ids
    )
    db.execute(f"DELETE FROM question WHERE id IN ({lstIds})", question_ids)

    answer_ids = [_[0] for _ in cur.fetchall()]

    lstIds = ",".join(["?"] * len(answer_ids))
    db.execute(f"DELETE FROM answer WHERE id IN ({lstIds})", answer_ids)
    db.execute("DELETE FROM quiz WHERE id = ?", (quiz_id,))

    db.commit()

    flask.flash("Quiz successfully deleted!", category="success")
    return flask.redirect(flask.url_for("admin.index", _anchor="quiz"))


@bp.route("/quiz/edit-<quiz_id>.html")
@secure
def edit_quiz(quiz_id):
    db = get_db()
    quiz = db.execute("SELECT * FROM quiz WHERE id=?", (quiz_id,)).fetchone()
    questions = db.execute(
        "SELECT * FROM question WHERE quiz_id=?", (quiz_id,)
    ).fetchall()
    answers = {}
    for question in questions:
        a = db.execute(
            "SELECT * FROM answer WHERE question_id=?", (question["id"],)
        ).fetchall()
        answers[question["id"]] = a

    return flask.render_template(
        "admin/edit_quiz.html", quiz=quiz, questions=questions, answers=answers
    )


@bp.route("/quiz/change-<quiz_id>.html", methods=("POST",))
@secure
def change_quiz(quiz_id):
    dctData = flask.request.get_json()
    # print(dctData, file=sys.stderr)

    if dctData["title"].strip() == "":
        return flask.jsonify({"status": "error", "msg": "Title is needed!"})
    elif len(dctData["questions"]) == 0:
        return flask.jsonify(
            {"status": "error", "msg": "At least 1 question is needed!"}
        )

    db = get_db()

    # Remove deleted questions and answers
    if dctData["del_answers"]:
        lstIds = ",".join(["?"] * len(dctData["del_answers"]))
        db.execute(f"DELETE FROM answer WHERE id IN ({lstIds})", dctData["del_answers"])

    if dctData["del_questions"]:
        lstIds = ",".join(["?"] * len(dctData["del_questions"]))
        db.execute(
            f"DELETE FROM question WHERE id IN ({lstIds})", dctData["del_questions"]
        )

    # Update quiz data
    db.execute(
        "UPDATE quiz SET title=?, body=?, public=? WHERE id=?",
        (dctData["title"], dctData["body"], dctData["public"], quiz_id),
    )

    # Update/create everything else
    for question in dctData["questions"]:
        # Add questions without id
        # Update questions with id
        if question["id"] is None:
            qid = db.execute(
                "INSERT INTO question (title, quiz_id) " "VALUES (?, ?)",
                (question["title"], quiz_id),
            ).lastrowid
        else:
            db.execute(
                "UPDATE question SET title=? WHERE id=?",
                (question["title"], question["id"]),
            )
            qid = question["id"]

        for answer in question["answers"]:
            # Add answers without id
            # Update answers with id
            if answer[0] is None:
                db.execute(
                    "INSERT INTO answer (title, correct, question_id) "
                    "VALUES (?, ?, ?)",
                    (answer[1], answer[2], qid),
                )
            else:
                db.execute(
                    "UPDATE answer SET title=?, correct=? WHERE id=?",
                    (answer[1], answer[2], answer[0]),
                )
    db.commit()

    return flask.jsonify({"status": "success", "msg": "Quiz successfully changed!"})


@bp.route("/game/create-<quiz_id>.html")
@secure
def create_game(quiz_id):
    code = "".join([random.choice(strChars) for _ in range(6)])
    db = get_db()
    db.execute(
        "INSERT INTO game (code, quiz_id, author_id) " "VALUES (?, ?, ?)",
        (code, quiz_id, flask.g.author["id"]),
    )
    db.commit()
    return flask.redirect(flask.url_for("admin.index", _anchor="game"))


@bp.route("/game/anonym-<quiz_id>.html")
@secure
def create_anonym(quiz_id):
    code = "".join([random.choice(strChars) for _ in range(6)])
    db = get_db()
    db.execute(
        "INSERT INTO game (code, quiz_id, author_id, anonymous) " "VALUES (?, ?, ?, ?)",
        (code, quiz_id, flask.g.author["id"], True),
    )
    db.commit()
    return flask.redirect(flask.url_for("admin.index", _anchor="game"))


@bp.route("/logout.html")
def logout():
    flask.session.clear()
    return flask.redirect(flask.url_for("admin.login"))


@bp.route("/")
@bp.route("/index.html")
@secure
def index():
    db = get_db()

    quizzes = db.execute(
        "SELECT q.*, COUNT(g.id) as played, COUNT(x.id) as questions, "
        "a.name as author, a.id as author_id FROM quiz q "
        "LEFT JOIN game g ON g.quiz_id=q.id "
        "LEFT JOIN question x ON x.quiz_id=q.id "
        "LEFT JOIN author a ON a.id=q.author_id "
        "WHERE q.author_id=? OR q.public=1 GROUP BY q.id",
        (flask.g.author["id"],),
    ).fetchall()

    games = db.execute(
        "SELECT g.* , q.*, COUNT(p.id) as players, "
        "a.name as author, a.id as author_id FROM game g "
        "LEFT JOIN player p ON p.game_id=g.id "
        "LEFT JOIN quiz q ON g.quiz_id=q.id "
        "LEFT JOIN author a ON a.id=g.author_id "
        "WHERE g.author_id=? GROUP BY g.id",
        (flask.g.author["id"],),
    ).fetchall()

    return flask.render_template("admin/index.html", quizzes=quizzes, games=games)


@socketio.on("show_results", namespace="/admin")
def show_results():
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session.get("game_id"),)
    ).fetchone()

    question = db.execute(
        "SELECT * FROM question WHERE quiz_id=? LIMIT ?,1",
        (game["quiz_id"], game["progress"]),
    ).fetchone()

    guesses = db.execute(
        "SELECT answer_id AS id, COUNT(answer_id) AS cnt FROM guess "
        "WHERE question_id=? AND game_id=? "
        "GROUP BY answer_id",
        (question["id"], game["id"]),
    ).fetchall()

    correct = db.execute(
        "SELECT id FROM answer " "WHERE question_id=? AND correct=1", (question["id"],)
    ).fetchall()

    correct = [_[0] for _ in correct]

    dctData = {"votes": {g["id"]: g["cnt"] for g in guesses}, "correct": correct}

    socketio.emit("show_results", dctData, namespace="/game", room=game["code"])


@socketio.on("enable_answers", namespace="/admin")
def enable_answers(timer):
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()
    question = db.execute(
        "SELECT * FROM question WHERE quiz_id=? LIMIT ?,1",
        (game["quiz_id"], game["progress"]),
    ).fetchone()
    answers = db.execute(
        "SELECT COUNT(id) AS count FROM answer WHERE question_id=?", (question["id"],)
    ).fetchone()

    # print(answers)

    if timer == "-1":
        timer = -1
        t = -1
    else:
        timer = int(timer)
        t = time.time() + timer + 2 * answers["count"]
    db.execute(
        "UPDATE game SET status=1, timer=? WHERE id=?",
        (
            t,
            flask.session["game_id"],
        ),
    )
    db.commit()

    progress = game["progress"]
    socketio.emit("enable", (".answer", timer), namespace="/game", room=game["code"])
    socketio.emit("reload", 0, namespace="/admin")
    if timer > 0:
        socketio.sleep(timer + 2 * answers["count"])
        game = db.execute(
            "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
        ).fetchone()
        if game["progress"] == progress:
            disable_answers()
        else:
            pass
            # print('not disabeling, next question')


@socketio.on("remove_player", namespace="/admin")
def remove_player(player_name):
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()
    player = db.execute(
        "SELECT * FROM player WHERE name=? AND game_id=?",
        (player_name, flask.session["game_id"]),
    ).fetchone()

    if player is not None and game is not None:
        db.execute(
            "DELETE FROM player WHERE game_id=? AND id=?", (game["id"], player["id"])
        )
        db.commit()
        socketio.emit("reload", 0, namespace="/game", room=game["code"])
        socketio.emit("reload", 0, namespace="/admin")


@socketio.on("restart_game", namespace="/admin")
def restart_game():
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()

    db.execute(
        "UPDATE game SET progress=0, timer=0, question_id=0, status=0, "
        "started=CURRENT_TIMESTAMP, ended=NULL WHERE id=?",
        (game["id"],),
    )
    db.execute("DELETE FROM guess WHERE game_id=?", (game["id"],))
    db.commit()

    socketio.emit("reload", 0, namespace="/game", room=game["code"])
    socketio.emit("reload", 0, namespace="/admin")


@socketio.on("reset_game", namespace="/admin")
def refresh_game():
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()

    db.execute(
        "UPDATE game SET progress=0, timer=0, question_id=0, status=0, "
        "started=NULL, ended=NULL WHERE id=?",
        (game["id"],),
    )

    db.execute("DELETE FROM guess WHERE game_id=?", (game["id"],))
    db.execute("DELETE FROM player WHERE game_id=?", (game["id"],))
    db.commit()

    socketio.emit("reload", 0, namespace="/game", room=game["code"])
    socketio.emit("reload", 0, namespace="/admin")


@socketio.on("send_msg", namespace="/admin")
def send_msg(msg):
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()
    if game is not None:
        # print(f'Sending message: {msg}')
        socketio.emit(
            "toast", {"type": "", "msg": msg}, namespace="/game", room=game["code"]
        )
    # else:
    #     print(f'Message not sent')


@socketio.on("disable_answers", namespace="/admin")
def disable_answers():
    db = get_db()
    db.execute(
        "UPDATE game SET status=2, timer=0 WHERE id=?", (flask.session["game_id"],)
    )
    db.commit()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()

    socketio.emit("disable", ".answer", namespace="/game", room=game["code"])
    socketio.emit("reload", 0, namespace="/admin")


@socketio.on("next_question", namespace="/admin")
def next_question():
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()

    db.execute(
        "UPDATE game SET status=0, timer=0, progress=progress+1 " "WHERE id=?",
        (flask.session["game_id"],),
    )
    db.commit()

    socketio.emit("reload", 0, namespace="/game", room=game["code"])
    socketio.emit("reload", 0, namespace="/admin")


@socketio.on("finish_game", namespace="/admin")
def finish_game():
    db = get_db()
    game = db.execute(
        "SELECT * FROM game WHERE id=?", (flask.session["game_id"],)
    ).fetchone()
    db.execute(
        "UPDATE game SET status=0, timer=0, ended=CURRENT_TIMESTAMP " "WHERE id=?",
        (flask.session["game_id"],),
    )
    db.commit()

    socketio.emit("reload", 0, namespace="/game", room=game["code"])
    socketio.emit("reload", 0, namespace="/admin")
