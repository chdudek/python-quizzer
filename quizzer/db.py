#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3 as sql
import click
import json
from flask import current_app as curapp
from flask import g
from flask.cli import with_appcontext, AppGroup
from werkzeug.security import generate_password_hash


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(qcli)


def get_db():
    if "db" not in g:
        g.db = sql.connect(curapp.config["DATABASE"], detect_types=sql.PARSE_DECLTYPES)
        g.db.row_factory = sql.Row

    return g.db


def close_db(e=None):
    db = g.pop("db", None)
    if db is not None:
        db.close()


def init_db():
    db = get_db()
    with curapp.open_resource("schema.sql") as f:
        db.executescript(f.read().decode("utf8"))


def init_data():
    db = get_db()
    db.execute(
        "INSERT INTO author (name, password, level) VALUES (?, ?,1)",
        ("Admin", generate_password_hash("admin")),
    )

    db.commit()


@click.command("init-db")
@with_appcontext
def init_db_command():
    init_db()
    init_data()
    click.echo("Database initialized!")


qcli = AppGroup("question")


@qcli.command("fromjson")
@click.argument("file")
@with_appcontext
def load_questions_json(file):
    with open(file, "r") as f:
        data = json.load(f)

    db = get_db()

    qid = db.execute(
        "INSERT INTO quiz (title, body, author_id) VALUES (?, ?, ?)",
        (data["title"], data["body"], 1),
    ).lastrowid

    for question in data["questions"]:
        q = db.execute(
            "INSERT INTO question (title, quiz_id) VALUES (?, ?)",
            (question["question"], qid),
        ).lastrowid
        for a, answer in enumerate(question["answers"]):
            db.execute(
                "INSERT INTO answer (title, correct, question_id) " "VALUES (?, ?, ?)",
                (answer, 1 if a == question["correct"] else 0, q),
            )
    db.commit()
    click.echo(f"Json questions loaded!")
