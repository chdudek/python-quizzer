#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import flask
import flask_socketio
from quizzer.db import get_db
from flask import current_app as curapp

socketio = flask_socketio.SocketIO(manage_session=False)


@socketio.on('message')
def handle_message(msg):
    flask_socketio.send(msg, broadcast=True)


@socketio.on('disconnect')
def disconnect():
    pass
