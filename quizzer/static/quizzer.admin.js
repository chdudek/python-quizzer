
let del_answers = []
let del_questions = []

$(document).ready(function(){
  // Connect to socketio admin namespace
  let sio = io.connect(`${location.protocol}//${document.domain}:${location.port}/admin`,
                       {secure: location.protocol == "https:"});

  // QRcode download
  $('.qrdownload').on('click', function() {
    let qr = $('<div></div>').qrcode({
      render: 'canvas',
      text: `${location.protocol}//${document.domain}:${location.port}/game/${$(this).attr('data-qrcode')}`
    }).children('canvas')[0];
    $(this).attr('download', `quizzer_game_qr_${$(this).attr('data-qrcode')}.png`);
    $(this).attr('href', qr.toDataURL());
  })

  // QRcode download
  $('.qrshow').on('click', function() {
    let qr = $('<div></div>').qrcode({
      render: 'canvas',
      text: `${location.protocol}//${document.domain}:${location.port}/game/${$(this).attr('data-qrcode')}`
    }).children('canvas')[0];
    $('.modal-title').html($(this).attr('data-qrcode'))
    $('.modal-body > .content').html(qr)
    $('.modal-footer > small').html(`${location.protocol}//${document.domain}:${location.port}/game/${$(this).attr('data-qrcode')}`)
    $('#qr-modal').addClass('active');
    // $(this).attr('download', `quizzer_game_qr_${$(this).attr('data-qrcode')}.png`);
    // $(this).attr('href', qr.toDataURL());
  })

  $('.modal-close').on('click', function() {
    $('#qr-modal').removeClass('active');
  })

  // Tabs
  $('.tab-item > a').on('click', function() {
    $('.tab-item > a').removeClass('active');
    $(this).addClass('active');
    $('.panel-body:visible').toggle();
    $(`#${$(this).attr('for')}`).toggle();
  })

  $('.tab_content').hide();
  let tab = window.location.hash;

  $(`.tab-item > a[href="${tab}"]`).trigger('click');
  if( $(`.tab-item > a[href="${tab}"]`).length == 0) {
    $('.tab-item > a').first().trigger('click');
  }

  $('button.enable_answers').on('click', function() {
    sio.emit('enable_answers', $(this).attr('data-timer'))
  });


  $('#restart_game').on('click', function() {
    sio.emit('restart_game');
  });

  $('#reset_game').on('click', function() {
    sio.emit('reset_game');
  });

  $('#disable_answers').on('click', function() {
    sio.emit('disable_answers')
  });

  $('#show_results').on('click', function() {
    sio.emit('show_results');
  });

  $('#finish_game').on('click', function() {
    sio.emit('finish_game');
  });

  $('#next_question').on('click', function() {
    sio.emit('next_question');
  });

  $('#lobby').on('click', '.remove', function() {
    sio.emit('remove_player', $(this).attr('data-name'))
  });

// Adding questions and answers

// Template for answers:
const answer = ({qid, aid, checked='', answer=''}) => `
  <div class="answer column py-1 col-sm-6 col-lg-3 text-left input-group" data-question="${qid}" data-answer="${aid}">
      <span class="input-group-addon">
        <input type="checkbox" name="correct-${qid}" data-answer="${aid}"${checked}>
      </span>
      <input type="text" class="form-input" name="answer-${qid}" placeholder="Answer" value="${answer}">
      <button class="btn del_answer btn-error input-group-btn" data-question="${qid}" data-answer="${aid}">
        <i class="fas fa-trash"></i>
      </button>
  </div>
`
// Template for questions:
const question = ({idx, qid, question=''}) => `
  <div data-question="${idx}" data-qid="${qid}" class="card question mt-1">
    <div class="card-header">
      <div class="card-subtitle text-gray">Question ${idx}</div>
      <div class="card-title h5">
        <input type="text" name="question-${idx}" class="form-input" placeholder="Question" value="${question}">
      </div>
    </div>
    <div class="card-body container">
      <div class="answers-${idx}" class="columns">

      </div>
    </div>
    <div class="card-footer text-right">
      <button data-question="${idx}" class="add_answer btn btn-sm btn-primary input-group-btn">
        <i class="fas fa-plus"></i> Add answer
      </button>
      <button data-question="${idx}" data-qid="${qid}" class="del_question btn btn-sm btn-error input-group-btn">
        <i class="fas fa-trash"></i> Delete question
      </button>
    </div>
  </div>
`

  $('#add_question').on('click', function() {
    let mx = 0
    $('.question').each((i, e) => {
      mx = Math.max(parseInt($(e).attr('data-question')), mx)
    })
    mx++
    $('#questions').append(question({idx: mx}))
  });

  $(document).on('click', '.del_question', function() {
    let q = $(this).attr('data-question')
    if ($(this).hasClass('old')) {
      del_questions.push(parseInt($(this).attr('data-qid')))
    }

    $(`.question[data-question="${q}"]`).remove()
  })

  $(document).on('click', '.add_answer', function() {
    let q = $(this).attr('data-question')
    let mx = 0
    $(`.answer[data-question="${q}"]`).each((i, e) => {
      mx = Math.max(parseInt($(e).attr('data-answer')), mx)
    })
    mx++

    $(`.answers-${q}`).append(answer({qid: q, aid: mx}))
  })

  $(document).on('click', '.del_answer', function() {
    let q = $(this).attr('data-question')
    let a = $(this).attr('data-answer')

    if ($(this).hasClass('old')) {
      del_answers.push(parseInt(a))
    }

    $(`.answer[data-question="${q}"][data-answer="${a}"]`).remove()
  })


  $('#add_quiz').on('click', function() {
    let quiz = {
      'title': $(`input[name="title"]`).val(),
      'body': $(`input[name="body"]`).val(),
      'public': $(`input[name="public"]`).is(':checked'),
      'questions': []
    }
    $('.question').each((i, e) => {
      let q = $(e).attr('data-question')
      let answers = []

      $(`input[name="answer-${q}"]`).each((i, e) => {
        let ans = $(e).val()
        let chk = $($(`input[name="correct-${q}"]`)[i]).is(':checked')
        answers.push([ans, chk])
      })

      quiz.questions.push({
        'title': $(`input[name="question-${q}"]`).val(),
        'answers': answers
      })
    })

    $.ajax({
      type: $('#form').attr('method'),
      url: $('#form').attr('action'),
      data: JSON.stringify(quiz),
      success: (e) => {
        makeToast(e.status, e.msg)
      },
      dataType: 'json',
      contentType : 'application/json'
    })
  })


  $('#change_quiz').on('click', function() {
    let quiz = {
      'title': $(`input[name="title"]`).val(),
      'body': $(`input[name="body"]`).val(),
      'public': $(`input[name="public"]`).is(':checked'),
      'questions': []
    }
    $('.question').each((i, e) => {
      let q = parseInt($(e).attr('data-question'))
      let answers = []

      $(`input[name="answer-${q}"]`).each((i, e) => {
        let a = parseInt($(e).attr('data-answer'))
        let ans = $(e).val()
        let chk = $($(`input[name="correct-${q}"]`)[i]).is(':checked')
        answers.push([a, ans, chk])
      })

      quiz.questions.push({
        'id': parseInt($(e).attr('data-qid')),
        'title': $(`input[name="question-${q}"]`).val(),
        'answers': answers
      })
    })

    quiz.del_questions = del_questions
    quiz.del_answers = del_answers


    $.ajax({
      type: $('#form').attr('method'),
      url: $('#form').attr('action'),
      data: JSON.stringify(quiz),
      success: (e) => {
        makeToast(e.status, e.msg)
      },
      dataType: 'json',
      contentType : 'application/json'
    })

  })

  sio.on('guess', function(data) {
    $('[id^=answer-]').attr('data-badge', '');
    for (let a in data) {
      $(`#answer-${a}`).attr('data-badge', data[a]);
    }
  })

  // User joined
  sio.on('joined', function(users, empty=false) {
    let cnt = parseInt($('#counter').html())
    if (empty) {
      $('#lobby').empty()
      cnt = 0
    }
    cnt += users.length
    $('#counter').html(cnt)
    for (let user of users) {
      $('#lobby').append(`<div class="chip" style="background-color: ${user.color};">${user.name} <a data-name="${user.name}" class="btn btn-clear remove" aria-label="Close" role="button"></a></div>`);
    }
  })

  $('#send_msg').on('click', function() {
    sio.emit('send_msg', $('#msg').val())
    $('#msg').val('')
  })

  $('#msg').on('keypress', function(e) {
    if (e.which === 13){
        sio.emit('send_msg', $('#msg').val())
        $('#msg').val('')
      }
  })


  // Reload the page after countdown
  sio.on('reload', function(secs) {
    console.log(`reload ${secs}s`);
    if (secs > 0){
      let time = obj.time;
      let inter = setInterval(function() {
          $('#timer').text(time);
          if (time==0) {
            window.location.reload();
          }
          time--;
      }, 1000);
    } else {
      window.location.reload();
    }
  })
})
