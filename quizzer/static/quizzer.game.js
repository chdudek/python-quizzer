let inter;
timer = function(t) {
  if ($.isNumeric(t)) {
    if (t == -1) {
      $('#timer').text('∞');
    } else {
      $('#timer').text(t);
      inter = setInterval(function() {
          if (t==0) {
            clearInterval(inter);
          } else {
            t--;
            $('#timer').text(t);
          }
      }, 1000);
    }
  }
}


$(document).ready(function(){
  // Connect to socketio with game namespace
  let sio = io.connect(`${location.protocol}//${document.domain}:${location.port}/game`,
                       {secure: location.protocol == "https:"});

  // User joined
  sio.on('joined', function(users, empty=false) {
    let cnt = parseInt($('#counter').html())
    if (empty) {
      $('#lobby').empty()
      cnt = 0
    }

    cnt += users.length
    $('#counter').html(cnt)

    if($('#lobby').length) {
      for (let user of users) {
        $('#lobby').append(`<div class="chip"><figure class="avatar avatar-sm" style="background-color: ${user.color};"></figure>${user.name}</div>`);
      }
    }
  })

  // Enable elements
  sio.on('enable', function(element, secs) {
    $(element).each(function(i) {
      $(this).prop('disabled', true);
      $(this).delay(2000*i).css({opacity: 0.0, visibility: "visible"}).animate({opacity: 0.5}, 1500)
    });

    setTimeout(function() {
      $(element).css({opacity: ''})
      $(element).prop('disabled', false);
      timer(secs);
    }, 2000*$(element).length)
  });

  // Disable elements
  sio.on('disable', function(element) {
    $(element).prop('disabled', true);
    clearInterval(inter);
    $('#timer').text('Time over');
  });

  // Show results
  sio.on('show_results', function(data) {
    $('.btn-primary').addClass('btn-error');
    $('.btn-primary').removeClass('btn-primary');

    for (let c of data.correct) {
      $(`button#answer-${c}`).removeClass('btn-error')
      $(`button#answer-${c}`).addClass('btn-success');
      $(`div#answer-${c}`).addClass('bg-success');
    }
    for (let a in data.votes) {
      $(`button#answer-${a}`).addClass('badge');
      $(`button#answer-${a}`).attr('data-badge', data.votes[a]);
      $(`div#answer-${a}`).children('div').addClass('badge');
      $(`div#answer-${a}`).children('div').attr('data-badge', data.votes[a]);
    }
  });

  // Reload the page after countdown
  sio.on('reload', function(secs) {
    console.log(`reload ${secs}s`)
    if (secs > 0) {
      let time = obj.time
      let inter = setInterval(function() {
          $('#timer').text(time)
          if (time==0) {
            window.location.reload()
          }
          time--
      }, 1000)
    } else {
      window.location.reload()
    }
  })

  // Join the game
  $('#join').on('click', function() {
    let c = $('#color').val()
    let u = $('#username').val()
    if (c === undefined) {
      c = '#000000'
    }
    if (u === undefined) {
      u = 'anonymous'
    }
    sio.emit('join',u ,c )
  })

  // Cast answer
  $('.answer').on('click', function() {
    $('.answer').removeClass('btn-primary')
    $(this).addClass('btn-primary')
    sio.emit('guess', $(this).attr('data-answer'))
  });

  // Make toast from socketio
  sio.on('toast', function(jsn) {
    makeToast(jsn.type, jsn.msg);
  });

  // Start timer
  timer($('#timer').text())

  // Result animation
  $($('div.bar-item').get().reverse()).each(function(i) {
    // $(this).css('width', $(this).attr('data-percent'))
    $(this).delay(3000*(i+1)).animate({width: `${$(this).attr('data-percent')}%`}, 1000, function() {
      $(this).text($(this).attr('data-name'))
      $(this).parent().parent().next().text($(this).attr('data-points'))
      // $(this).parent().parent().prev().text($(this).attr('data-points'))
    });
  })
});
