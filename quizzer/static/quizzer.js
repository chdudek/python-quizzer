
function makeToast(typ, msg) {
  let cnt = `<div class="toast p-centered toast-${typ}"><button class="fa fa-close btn btn-clear float-right"></button>${msg}</div>`
  $(cnt).hide().appendTo("#toaster").fadeIn(750)
    .delay(3000).queue(function() {
        $(this).slideUp(500);
        $(this).dequeue();
    });
}

$(document).ready(function(){
  // Connect to socketio
  let sio = io.connect(`${location.protocol}//${document.domain}:${location.port}`,
                       {secure: location.protocol == "https:"});

  // create QRcodes
  $('.qrcode').each(function() {
    $(this).qrcode({
      render: 'canvas',
      text: `${location.protocol}//${document.domain}:${location.port}/game/${$(this).attr('data-qrcode')}`
    });
  });

  // Random color and color picker
  if($('#color').length > 0) {
    var c = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
    $('#color').val(c);
    $('#color').spectrum({
      color: c,
      replacerClassName: 'form-input',
      change: function(color) {
        $('#color').val(color.toHexString());
      }
    });
  }

  // Reload the page after countdown
  sio.on('reload', function(secs) {
    if (secs > 0){
      let time = obj.time;
      let inter = setInterval(function() {
          $('#timer').text(time);
          if (time==0) {
            window.location.reload();
          }
          time--;
      }, 1000);
    } else {
      window.location.reload();
    }
  });

  // Toasts
  // Make toast from socketio
  sio.on('toast', function(jsn) {
    makeToast(jsn.type, jsn.msg);
  });
  sio.on('goto', function(url) {
    window.location = `${location.protocol}//${document.domain}:${location.port}${url}`;
  });

  $("#toaster").on("click", ".toast .btn-clear", function() {
      $(this).closest(".toast").dequeue();
      $(this).closest(".toast").slideUp();
  });

  //
  $(".toast").each(function() {
      $(this).delay(3000).queue(function() {
          $(this).fadeOut(500).slideUp();
          $(this).dequeue();
      });
  });

  // Disconnect to socketio
  $(window).on("beforeunload", function() {
      sio.send("disconnect");
  });


});
